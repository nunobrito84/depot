require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  fixtures :products

  test "There should be at least two languages" do
    assert LANGUAGES.count >= 2, "Should have at least two languages"
  end
  
  test "Buying a Product" do
    # Reset the session data to start the story
    LineItem.delete_all
    Order.delete_all
    ruby_book = products(:ruby) #get the book to a local var to make it easier to read
    # Get Store Index
    get("/")
    assert_response :success
    assert_template "index"
    # Add product to cart using AJAX
    xml_http_request :post, '/line_items', product_id: ruby_book.id
    assert_response :success
    # Check if the product added is now in the cart 
    cart = Cart.find(session[:cart_id])
    assert_equal 1, cart.line_items.size
    assert_equal ruby_book, cart.line_items[0].product
    # Check out
    get "/orders/new"
    assert_response :success
    assert_template "new"
    # Insert client data to finalize the checkout
    post_via_redirect "/orders",
      order: { name: "Dave Thomas",
               address: "123 The Street",
               email: "dave@example.com",
               pay_type: "Check" }
    assert_response :success
    # Check if after the test, the user is redirected to the main index page
    assert_template "index"
    #puts "estamos aqui"
    #puts "cartId: ", session[:cart_id]
    cart = Cart.find(session[:cart_id])
    assert_equal 0, cart.line_items.size 
    # Lets check if the data in the database is correct 
    orders = Order.all
    assert_equal 1, orders.size
    order = orders[0]
    
    assert_equal "Dave Thomas", order.name
    assert_equal "123 The Street", order.address
    assert_equal "dave@example.com", order.email
    assert_equal "Check", order.pay_type
    
    assert_equal 1, order.line_items.size
    line_item = order.line_items[0]
    assert_equal ruby_book, line_item.product
    # Finally lets check if the email is correctly addressed
    mail = ActionMailer::Base.deliveries.last
    assert_equal ["dave@example.com"], mail.to
    assert_equal 'Sam Ruby <depot@example.com>', mail[:from].value
    assert_equal "Pragmatic Store Order Confirmation", mail.subject
  end
  
  test "Should deny access after logout" do
    #Perform logout
    delete "/logout"
    assert_redirected_to :controller => 'store', :locale => 'en'
    #Now try to get the users list and check if it redirects to the login url
    get "/users/"
    assert_redirected_to :controller => "sessions", :action => "new", :locale => :en
  end  
end