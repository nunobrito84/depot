require 'test_helper'

class StoreControllerTest < ActionController::TestCase 
  test "should get index" do  
    get :index , {:locale => 'en'}
    assert_response :success 
    assert_select '#columns #side a', minimum: 4 
    assert_select '#main .entry', 3 
    assert_select 'h3', 'Programming Ruby 1.9' 
    assert_select '.price', /\$[,\d]+\.\d\d/ 
  end 
  
  test "markup needed for store.js.coffee is in place" do
    get :index, {:locale => 'en'}
    assert_select '.store .entry > img', 3
    assert_select '.entry input[type=submit]', 3
  end
  
  test "Should only get the spanish titles" do 
    get :index, {:locale => 'es'}
    assert_response :success
    puts "Count: #{Product.where("locale = ?", LANGUAGES[1][1]).count}"
    assert_select '#main .entry', Product.where("locale = ?", LANGUAGES[1][1]).count
    #assert_equal Product.where("locale = ?", LANGUAGES[1][1]).count, 1
  end
  
end 
