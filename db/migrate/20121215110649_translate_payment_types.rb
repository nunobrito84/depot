class TranslatePaymentTypes < ActiveRecord::Migration
def self.up
    say_with_time "Updating payment type on orders..." do
      Order.all.each do |order|
        new_type = case order.pay_type
                     when "Credit Card" then
                       "cc" 
                     when "Check" then
                       "check" 
                     when "Purchase Order" then
                       "po" 
                     else  
                       "??"
                   end
        order.update_attribute :pay_type, new_type
      end
    end
    say_with_time "Updating payment type on payment_types..." do
      PaymentType.all.each do |paytype|
        new_name = case paytype.name
                     when "Credit Card" then
                       "cc" 
                     when "Check" then
                       "check" 
                     when "Purchase Order" then
                       "po"
                     else "??" 
                   end
        paytype.update_attribute :name, new_name
      end
    end
  end

  def self.down
    say_with_time "Updating payment type down on orders..." do
      Order.all.each do |order|
        new_type = case order.pay_type
                     when "cc" then
                       "Credit Card" 
                     when "check" then
                       "Check" 
                     when "po" then
                       "Purchase Order"
                     else "??" 
                   end
        order.update_attribute :pay_type, new_type
      end
    end
    say_with_time "Updating payment type down on payment_types..." do
      PaymentType.all.each do |paytype|
        new_name = case paytype.name
                     when "cc" then
                       "Credit Card" 
                     when "check" then
                       "Check" 
                     when "po" then
                       "Purchase Order"
                     else "??"
                   end
        paytype.update_attribute :name, new_name
      end      
    end
  end
end
