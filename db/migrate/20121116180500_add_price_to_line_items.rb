class AddPriceToLineItems < ActiveRecord::Migration
  #def change
  #  add_column :line_items, :price, :decimal, precision: 8, scale: 2
  #end 
  def up
    add_column :line_items, :price, :decimal, precision: 8, scale: 2
    LineItem.all.each do |li|
      li.update_column :price, li.product.price
    end
  end 
  def down
    remove_column :line_items, :price
  end
end
