class AddLocaleToProducts < ActiveRecord::Migration
  def change
    add_column :products, :locale, :string, default: I18n.default_locale

  end
end
