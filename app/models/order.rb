class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy
  validates :name, :address, :email, presence: true
  #PAYMENT_TYPES = [ "Check", "Credit Card", "Purchase order" ]
  #validates :pay_type, inclusion: PAYMENT_TYPES
  #validates :pay_type, :inclusion => PaymentType.names
  validates_each :pay_type do |model, attr, value|
    if !PaymentType.names.include?(value)
      model.errors.add(attr, "Payment type not on the list") 
    end
  end
  
  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil #This is necessary to avoid the line_item from being destroyed when deleting the cart... because of the foreign key delete feature
      line_items << item
    end 
  end 
end 
