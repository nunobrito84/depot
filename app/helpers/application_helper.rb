# encoding: utf-8

module ApplicationHelper
  def hidden_div_if(condition, attributes = {}, &block)
    if condition
      attributes["style"] = "display: none"
    end
    content_tag("div", attributes, &block)
  end
  
  def number_to_local_currency(value)
    conversionVal = 1.0
    begin
      conversionVal = t('number.currency.format.conversionvalue', :raise => I18n::MissingTranslationData).to_f
    rescue I18n::MissingTranslationData
      logger.error "t('number.currency.format.conversionvalue') missing for the language: #{I18n.locale}" 
    end  
    return number_to_currency(value*conversionVal, :locale => I18n.locale)
  end
  
end
