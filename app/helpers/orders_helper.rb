module OrdersHelper
  
  def payment_method_locale(nameArray)
    newArray = Array.new
    nameArray.each do |paymentType|
      newArray.push([t("payment." + paymentType), paymentType])
    end
    return options_for_select(newArray)
  end

end
