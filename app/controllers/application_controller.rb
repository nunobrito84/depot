require 'digest/md5'

class ApplicationController < ActionController::Base
  before_filter :set_i18n_locale_from_params
  before_filter :authorize
  protect_from_forgery
  
  REALM = "SuperSecret" 
  USERS = { "test1" => "123", #plain text password  
            "test2" => Digest::MD5.hexdigest(["test2", REALM, "1234"].join(":")) }  #ha1 digest password

  private 
    def current_cart
      Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      cart = Cart.create
      session[:cart_id] = cart.id
      cart
    end
    
  private 
    def authorize_digest
      authenticate_or_request_with_http_digest(REALM) do |username|
        USERS[username]
      end
    end      
    
  private     
    def authorize 
      unless User.find_by_id(session[:user_id]) 
        if User.count > 0
          redirect_to login_url, notice: 'Please log in'
        else  
          if ((request.path_parameters[:controller] == 'users') && 
              (request.path_parameters[:action] == 'new' || request.path_parameters[:action] == 'create'))
          ;
          else
              redirect_to(:controller => 'users', :action => 'new')
          end
        end 
      end
    end
  protected
    def set_i18n_locale_from_params
      if params[:locale]
        if I18n.available_locales.include?(params[:locale].to_sym)
          I18n.locale = params[:locale]
        else
          flash.now[:notice] = "#{params[:locale]} translation not available"
          logger.error flash.now[:notice]
        end
      end
    end 
    def default_url_options
      { locale: I18n.locale }
    end    
end
