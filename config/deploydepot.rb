require "bundler/capistrano"

set :user, 'farmados' # The SSH username you are logging into the server(s) as. Using SSH Keys is recommended.
#set :password, '' # The SSH password. This is optional. By default password will be prompted. Using SSH Keys is recommended. 

set :use_sudo, false # If the user have sudo access, you can set this to true.
set :application, "depot" # Your application name
set :deploy_to, "/home/#{user}/#{application}" # Path in the server where the app will be deployed
set :repository,  "http://nunobrito84@bitbucket.org/nunobrito84/depot.git" # Your svn checkout or git clone url

set :scm, :git # specify your version control system (subversion, mercurial etc) 

#If you have password access to the repo, uncomment the lines below.
#Your repository 
#set :scm_password, "nunobrito84@gmail.com" # Your repository username. Using SSH keys is recommended.
#set :scm_user, "repo_username" # Your repository password. Using SSH keys is recommended.
#set :branch, "master"

set :bundle_dir, "#{shared_path}/vendor/bundle" #bundle path 
set :normalize_asset_timestamps, false # if you're using asset pipeline, set this to false#
default_run_options[:pty] = true  # if you get stdin: is not a tty error, uncomment it

role :web, "farmadose.railsplayground.net"                          # Your HTTP server, Apache/etc
role :app, "farmadose.railsplayground.net"                          # This may be the same as your `Web` server
role :db,  "farmadose.railsplayground.net", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here" # optional

# if you want to clean up old releases on each deploy uncomment this:
# set :keep_releases, 5 
after "deploy:restart", "deploy:cleanup" 

# if you're still using the script/reaper helper you will need 
# these http://github.com/rails/irs_process_scripts 

#If you want to change the ownership and permissions after deploy uncomment the task below
after "deploy:update_code" do
  run "chmod 755 #{release_path} -R"
  run "chown -R #{user}:#{user} #{release_path}"
end 

namespace :deploy do
  desc "cold deploy" 
    task :cold do
    update
    passenger::restart
  end
  
  desc "Restart Passenger" 
    task :restart do
      passenger::restart
  end
end
namespace :passenger do
  desc "Restart Passenger" 
    task :restart do
      run "cd #{current_path} && touch tmp/restart.txt" 
  end
end